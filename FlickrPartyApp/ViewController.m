//
//  ViewController.m
//  FlickrPartyApp
//
//  Created by Angel Boullon on 17/6/15.
//  Copyright (c) 2015 Angel Boullon. All rights reserved.
//

#import "ViewController.h"
#import "PhotoViewController.h"
#import "UIImageView+WebCache.h"

@interface ViewController ()

@property (nonatomic) NSURLConnection *connection;
@property (nonatomic) NSMutableData *dataFlickr;
@property (nonatomic) NSInteger connectionPage;

@end


@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    self.navigationController.navigationBar.topItem.title = @"Flickr Party";
    
    self.view.backgroundColor =[UIColor whiteColor];
    
    [self loadDataFromFlickrWithPage:1];
    
    //Collection view initialization
    self.view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    
    layout.sectionInset = UIEdgeInsetsMake(20, 10, 10, 10);
    layout.itemSize = CGSizeMake(90, 120);
    
    self.photoCollectionView=[[UICollectionView alloc] initWithFrame:self.view.frame collectionViewLayout:layout];
    
    [self.photoCollectionView setDataSource:self];
    [self.photoCollectionView setDelegate:self];
    
    [self.photoCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [self.photoCollectionView setBackgroundColor:[UIColor whiteColor]];
    
    self.photosPerPage = 10;
    
    [self.view addSubview:self.photoCollectionView];
}


#pragma mark- Memory
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- REST Request
- (void)loadDataFromFlickrWithPage:(int) page{
    NSString *stringURLFlickr = [NSString stringWithFormat:@"https://api.flickr.com/services/rest/?format=json&nojsoncallback=?&method=flickr.photos.search&tags=party&extras=description,owner_name,url_s,url_l,url_o&api_key=563344ad88a53da93b45ff61cd25b8a5&page=%d", page];
    
    NSURL* urlFlickr = [[NSURL alloc] initWithString:stringURLFlickr];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:urlFlickr
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:5];
    
    self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    self.connectionPage = page;
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    self.dataFlickr = [NSMutableData new];
    [self.dataFlickr setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [self.dataFlickr appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    NSLog(@"ERROR: %@",error);
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    NSError *error = nil;
    
    NSDictionary *flickrDictionary = [NSJSONSerialization JSONObjectWithData:self.dataFlickr options:NSJSONReadingMutableContainers error:&error];
        
    if (error || !flickrDictionary || ![flickrDictionary isKindOfClass:[NSDictionary class]]){
            NSLog(@"ERROR: It was not possible to parse the request. Error = %@", error);
            UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Please try again"
                                                             delegate:nil
                                                    cancelButtonTitle:@"Acept"
                                                    otherButtonTitles:nil];
            [message show];
    } else {
        //NSLog(@"%@",flickrDictionary);
        self.photosArray =  flickrDictionary[@"photos"][@"photo"];
        self.photosPerPage = [flickrDictionary[@"photos"][@"perpage"] integerValue];
        
        [self.photoCollectionView reloadData];
    }
}

#pragma mark- UICollection DataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    //return self.photosPerPage;
    return 15;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    cell.backgroundColor = [UIColor lightGrayColor];
    

    
    NSURL *URLPhoto = [NSURL URLWithString:[self.photosArray[indexPath.row] valueForKey:@"url_s"]];
    
    if (URLPhoto)
    {
        NSLog(@"Loading %@",URLPhoto);
    
        UIImageView *imageView = [[UIImageView alloc] init];
        
        [imageView sd_setImageWithURL:URLPhoto];
        
        imageView.frame = cell.contentView.bounds;
        [imageView setContentMode:UIViewContentModeScaleAspectFit];
        
        [cell.contentView addSubview:imageView];
    }
    return cell;
}

/*
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
}
*/

#pragma mark- UICollection Delegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    PhotoViewController *photoViewController = [[PhotoViewController alloc] init];
    
    photoViewController.photoData = self.photosArray[indexPath.row];
    
    [self.navigationController pushViewController:photoViewController animated:YES];
}


@end
