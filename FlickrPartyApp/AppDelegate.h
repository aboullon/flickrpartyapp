//
//  AppDelegate.h
//  FlickrPartyApp
//
//  Created by Angel Boullon on 17/6/15.
//  Copyright (c) 2015 Angel Boullon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

