//
//  ViewController.h
//  FlickrPartyApp
//
//  Created by Angel Boullon on 17/6/15.
//  Copyright (c) 2015 Angel Boullon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic,strong) UICollectionView *photoCollectionView;

@property (nonatomic,strong) NSArray *photosArray;
@property (nonatomic) NSInteger photosPerPage;

@end

