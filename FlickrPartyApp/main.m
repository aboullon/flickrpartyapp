//
//  main.m
//  FlickrPartyApp
//
//  Created by Angel Boullon on 17/6/15.
//  Copyright (c) 2015 Angel Boullon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
