//
//  PhotoViewController.h
//  FlickrPartyApp
//
//  Created by Angel Boullon on 22/6/15.
//  Copyright (c) 2015 Angel Boullon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoViewController : UIViewController

@property (nonatomic,strong) NSDictionary* photoData;
@property (nonatomic,strong) UIImageView *photoImageView;
@property (nonatomic,strong) UIImage *photo;

@end
