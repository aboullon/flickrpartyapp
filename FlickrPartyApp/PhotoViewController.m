//
//  PhotoViewController.m
//  FlickrPartyApp
//
//  Created by Angel Boullon on 22/6/15.
//  Copyright (c) 2015 Angel Boullon. All rights reserved.
//

#import "PhotoViewController.h"
#import "UIImageView+WebCache.h"

@implementation PhotoViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.navigationItem.title = [self.photoData valueForKey:@"title"];
    
    self.photoImageView = [[UIImageView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    NSURL *URLPhoto = [NSURL URLWithString:[self.photoData valueForKey:@"url_l"]];
    
    NSLog(@"%@",self.photoData);
    
    [self.photoImageView sd_setImageWithURL:URLPhoto];
    [self.photoImageView setContentMode:UIViewContentModeScaleAspectFit];

    [self.view addSubview:self.photoImageView];
    
    self.view.backgroundColor =[UIColor lightGrayColor];
}


@end
